# Утилита Renamer

##  Описание
Утилита, написанная на Python, для выборочного удаления слов в именах
файлов и директорий.

## Установка на Linux
1. Предварительно необходимо установить Python 3.
2. Устанавливаем необходимые пакеты для работы программы.
```
pip3 install -r requirements.txt
```
3. Переходим в директорию с утилитой, делаем файл исполняемым
и копируем его в директорию /usr/local/bin.
```
cd renamer
chmod +x renamer.py
sudo cp renamer.py /usr/local/bin/renamer
```

## SYNOPSIS
```
    renamer [OPTION]... [DIRECTORY]... 
```

## Опции
Рекурсивное изменение имён:
```
    -r, -R, --recursively 
```
Создание лога 'renamer_log.txt' в домашней директории пользователя.
Формат лога: "old file name ===> new file name":
```
    -log, -l
```
Слова для удаления:
```
    -w, --words 'WORD'
```
Слова добавляем без пробелов.

## Пример:
Рекурсивно удаляем 'XYZ' в именах директории /path/to/directory.
```
renamer --words 'XYZ' -r path/to/directory
```
Что было:
```
    /directory
         fileXYZ
         another_directory/
             another_file
             XYZ_another_file
             one_more_XYZ_directory/
```
Что стало:
```
    /directory
         file
         another_directory/
             another_file
             _another_file
             1one_more__directory/
```
