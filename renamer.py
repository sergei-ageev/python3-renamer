#!/usr/bin/env python3
"""
Utility for selective deleting phrases and words in names of files and directories
"""

from pathlib import Path
import argparse
import os

def cli_interface_arguments(parser):
    """
    Arguments 
    """

    # Argument 'recursively' for command 'delete'
    parser.add_argument('-r', '-R', '--recursively',
                               help='Do it recursively',
                               action='store_true')

    # Argument 'file' for command 'delete'
    parser.add_argument('directory',
                               type=str,
                               help='Input directory')

    parser.add_argument('--words', '-w',
                               type=str,
                               help='Words for deletion')

    parser.add_argument('--log', '-l',
                               help='Save names of modified files in log. Log is stored in your home directory.',
                               action='store_true')

def cli_interface():
    """
    Argparse construction making the parser.
    """
    parser = argparse.ArgumentParser(description='Utility for selective deleting symbols in names of files and directories')
    cli_interface_arguments(parser)
    return parser.parse_args()

def command_action(args):
    """
    What will happen?
    """

    def renamer(current_file_path, new_file_name, log):
        """
        Function for renaming files and directories
        """

        # Make file if we use --log
        def renamer_log_action(current_file_path, new_file_name):
            """
            Write modified files to the log
            """
            with open(f'{os.environ["HOME"]}/renamer_log.txt', 'a') as log_file:
                log_file.write(f'{current_file_path} ===> {current_file_path.parent}/{new_file_name}\n')

        try:
            # Delete --words in file's name
            current_file_path.rename(f'{current_file_path.parent}/{new_file_name}')
        except PermissionError:
            print(f'{current_file_path.parent} - permission denied. Change permissions of the directory and try again.')

        # If we use --log, make log file in script directory
        if log:
            renamer_log_action(current_file_path, new_file_name)

    dir_path = Path(args.directory)
    # Check is directory exist?
    if dir_path.exists():
        # Check is the file a directory?
        if dir_path.is_dir():
            # What will happen if we enter command with argument 'recursively'
            if args.recursively:
                # Find all files with mask --words recursively
                for current_file_path in dir_path.rglob('*' + args.words + '*'):
                    # Split directory by / and take name of the file + replace --words to nothing
                    new_file_name = str(current_file_path).split('/')[-1].replace(args.words, '').strip()
                    try:
                        renamer(current_file_path, new_file_name, args.log)
                    except OSError:
                        print('''OSError - try to take deeper directory or rename directories manually.
                                Then rerun script.''')
            else:
                # Find all files with mask --words in the directory without recursion
                for current_file_path in dir_path.glob('*' + args.words + '*'):
                    # Split directory by / and take name of the file + replace --words to nothing
                    new_file_name = str(current_file_path).split('/')[-1].replace(args.words, '').strip()
                    renamer(current_file_path, new_file_name, args.log)
        else:
            print('It is file. Try again. Enter a name of a directory.')
    else:
        print('Directory is not present. Try again.')

if __name__ == '__main__':
    args = cli_interface()
    command_action(args)
